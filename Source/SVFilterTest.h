/*
  ==============================================================================

    SVFilterTest.h
    Created: 1 Nov 2021 3:24:44pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "SVFilter.h"

class SVFilterTest : public juce::UnitTest
{
public:
    SVFilterTest() : UnitTest("SVFilter testing") {}

    void runTest() override
    {
        SVFilter filter;
        juce::AudioBuffer<float> buffer(2, 256);
        for (int sample = 0; sample < buffer.getNumSamples(); sample++)
        {
            for (int channel = 0; channel < buffer.getNumChannels(); channel++)
            {
                buffer.setSample(channel, sample, 1.0f);
            }
        }
        juce::dsp::AudioBlock<float> block(buffer);
        float cutoff = 1500.0f;
        float q = 1.0f;
        float normalMod = 10.0f;
        float smallMod = 0.01f;
        float largeMod = 100.0f;

        beginTest("Filter init");
        filter.prepare(44100.0f);
        expectEquals(filter.getSampleRate(), 44100.0f);
        expectEquals(filter.getCutoff(), 0.0f);
        expectEquals(filter.getFilterTypeString(), filter.FilterTypeToString(SVFilter::FilterType::LowPass));

        beginTest("Set filter without modulation");
        filter.setFilter(cutoff, q);
        expectEquals(filter.getCutoff(), cutoff);
        expectEquals(filter.getQ(), q);

        beginTest("Filter process");
        filter.process(block);
        expectNotEquals(filter.getZ1(0), 0.0f);
        expectNotEquals(filter.getZ1(1), 0.0f);
        expectNotEquals(filter.getZ2(0), 0.0f);
        expectNotEquals(filter.getZ2(1), 0.0f);

        beginTest("Filter reset");
        filter.reset();
        expectEquals(filter.getZ1(0), 0.0f);
        expectEquals(filter.getZ1(1), 0.0f);
        expectEquals(filter.getZ2(0), 0.0f);
        expectEquals(filter.getZ2(1), 0.0f);

        beginTest("Set filter with modulation");
        filter.reset();
        filter.setFilter(cutoff, q, normalMod);
        expectEquals(filter.getCutoff(), cutoff * normalMod);

        filter.setFilter(cutoff, q, smallMod);
        expectEquals(filter.getCutoff(), 20.0f);

        filter.setFilter(cutoff, q, largeMod);
        expectEquals(filter.getCutoff(), 20000.0f);
        
        beginTest("Set filter type");
        filter.reset();
        filter.setFilterType(0);
        expectEquals(filter.getFilterTypeString(), filter.FilterTypeToString(SVFilter::FilterType::LowPass));

        filter.setFilterType(1);
        expectEquals(filter.getFilterTypeString(), filter.FilterTypeToString(SVFilter::FilterType::HighPass));

        filter.setFilterType(2);
        expectEquals(filter.getFilterTypeString(), filter.FilterTypeToString(SVFilter::FilterType::BandPass));

    }
};

// Creating a static instance will automatically add the instance to the array
// returned by UnitTest::getAllTests(), so the test will be included when you call
// UnitTestRunner::runAllTests()
static SVFilterTest filter_test;