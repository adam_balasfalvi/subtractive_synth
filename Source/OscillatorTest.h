/*
  ==============================================================================

    OscillatorTest.h
    Created: 1 Nov 2021 3:24:08pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "Oscillator.h"

class OscillatorTest : public juce::UnitTest
{
public:
    OscillatorTest() : UnitTest("Oscillator testing") {}

    void runTest() override
    {
        Oscillator oscillator;
        juce::dsp::ProcessSpec spec;
        spec.maximumBlockSize = 256;
        spec.sampleRate = 44100.0f;
        spec.numChannels = 2;

        int c5_midi = 72;
        float c5_freq = 523.25f;

        beginTest("Oscillator prepare to play");
        oscillator.prepareToPlay(spec);
        expectEquals(oscillator.getSampleRate(), 44100.0f);

        beginTest("Set frequency");
        oscillator.resetState();
        oscillator.setWaveFrequency(c5_midi);
        expect(juce::isWithin<float>(oscillator.getFreq(), c5_freq, 0.1f));
        expect(juce::isWithin<float>(oscillator.getFrequency(), c5_freq, 0.1f));

        beginTest("Set negative frequency");
        oscillator.resetState();
        oscillator.setModulation(-c5_freq - 1.0f);
        oscillator.setWaveFrequency(c5_midi);
        expect(juce::isWithin(oscillator.getFreq(), 1.0f, 0.1f));
        expect(juce::isWithin(oscillator.getFrequency(), 1.0f, 0.1f));

        beginTest("Calculate c constant with negative frequency");
        oscillator.resetState();
        oscillator.prepareToPlay(spec);
        oscillator.setModulation(-c5_freq - 1.0f);
        oscillator.setWaveFrequency(c5_midi);
        float c = static_cast<float>(spec.sampleRate / (4.0f * oscillator.getFreq() * (1.0f - oscillator.getFreq() / spec.sampleRate)));
        expect(juce::isWithin(oscillator.getC(), c, 0.1f));

        beginTest("Set negative modulation");
        oscillator.resetState();
        oscillator.setModulation(-c5_freq);
        expect(juce::isWithin(oscillator.getModulation(), -c5_freq, 0.1f));

        beginTest("Phase shift");
        oscillator.resetState();
        float phase = oscillator.phaseShift(-juce::MathConstants<float>::pi, juce::MathConstants<float>::pi);
        expectEquals(phase, 0.0f);

        phase = oscillator.phaseShift(0.0f, juce::MathConstants<float>::pi);
        expectEquals(phase, juce::MathConstants<float>::pi);

        phase = oscillator.phaseShift(juce::MathConstants<float>::pi, juce::MathConstants<float>::pi);
        expectEquals(phase, 0.0f);

        phase = oscillator.phaseShift(-juce::MathConstants<float>::pi, -juce::MathConstants<float>::pi);
        expectEquals(phase, 0.0f);

        phase = oscillator.phaseShift(0.0f, -juce::MathConstants<float>::pi);
        expectEquals(phase, -juce::MathConstants<float>::pi);

        phase = oscillator.phaseShift(juce::MathConstants<float>::pi, -juce::MathConstants<float>::pi);
        expectEquals(phase, 0.0f);

        beginTest("Frequency double");
        oscillator.resetState();
        float halfPhase = oscillator.freqDouble(juce::MathConstants<float>::pi);
        expectEquals(halfPhase, juce::MathConstants<float>::pi);

        halfPhase = oscillator.freqDouble(0.0f);
        expectEquals(halfPhase, -juce::MathConstants<float>::pi);

        halfPhase = oscillator.freqDouble(-juce::MathConstants<float>::pi);
        expectEquals(halfPhase, -juce::MathConstants<float>::pi);

        beginTest("Saw wave trivial");
        oscillator.resetState();
        expectEquals(oscillator.sawWaveTrivial(-juce::MathConstants<float>::pi), -1.0f);
        expectEquals(oscillator.sawWaveTrivial(0.0f), 0.0f);
        expectEquals(oscillator.sawWaveTrivial(juce::MathConstants<float>::pi), 1.0f);

        beginTest("Saw wave DPW");
        oscillator.resetState();
        oscillator.prepareToPlay(spec);
        oscillator.setWaveFrequency(c5_midi);

        float step = juce::MathConstants<float>::twoPi * c5_freq / oscillator.getSampleRate();
        int numStep = static_cast<int>(std::floor(1 / step));

        float sawTrivial = oscillator.sawWaveTrivial(-juce::MathConstants<float>::pi);
        float sawDPW = oscillator.sawWaveDPW_0(-juce::MathConstants<float>::pi);

        for (int i = 1; i < numStep; i++)
        {
            sawTrivial = oscillator.sawWaveTrivial(-juce::MathConstants<float>::pi + step * i);
            sawDPW = oscillator.sawWaveDPW_0(-juce::MathConstants<float>::pi + step * i);
            expect(juce::isWithin(sawTrivial, sawDPW, 0.1f));
        }

        beginTest("Square wave trivial");
        oscillator.resetState();
        expectEquals(oscillator.squareWaveTrivial(-juce::MathConstants<float>::pi), -1.0f);
        expectEquals(oscillator.squareWaveTrivial(0.0f), 1.0f);
        expectEquals(oscillator.squareWaveTrivial(juce::MathConstants<float>::pi), 1.0f);

        beginTest("Square wave DPW");
        oscillator.resetState();
        oscillator.prepareToPlay(spec);
        oscillator.setWaveFrequency(c5_midi);

        float squareTrivial = oscillator.squareWaveTrivial(-juce::MathConstants<float>::pi);
        float squareDPW = oscillator.squareWaveDPW(-juce::MathConstants<float>::pi);

        for (int i = 1; i < numStep; i++)
        {
            squareTrivial = oscillator.squareWaveTrivial(-juce::MathConstants<float>::pi + step * i);
            squareDPW = oscillator.squareWaveDPW(-juce::MathConstants<float>::pi + step * i);
            expect(juce::isWithin(squareTrivial, squareDPW, 0.1f));
        }

        beginTest("Triangle wave trivial");
        oscillator.resetState();
        expectEquals(oscillator.triangleWaveTrivial(-juce::MathConstants<float>::pi), -1.0f);
        expectEquals(oscillator.triangleWaveTrivial(0.0f), 1.0f);
        expectEquals(oscillator.triangleWaveTrivial(juce::MathConstants<float>::pi), -1.0f);

        beginTest("Triangle wave DPW");
        oscillator.resetState();
        oscillator.prepareToPlay(spec);
        oscillator.setWaveFrequency(c5_midi);

        float triangleTrivial = oscillator.triangleWaveTrivial(-juce::MathConstants<float>::pi);
        float triangleDPW = oscillator.triangleWaveDPW(-juce::MathConstants<float>::pi);

        for (int i = 1; i < numStep; i++)
        {
            triangleTrivial = oscillator.triangleWaveTrivial(-juce::MathConstants<float>::pi + step * i);
            triangleDPW = oscillator.triangleWaveDPW(-juce::MathConstants<float>::pi + step * i);
            expect(juce::isWithin(triangleTrivial, triangleDPW, 0.1f));
        }
    }
};

// Creating a static instance will automatically add the instance to the array
// returned by UnitTest::getAllTests(), so the test will be included when you call
// UnitTestRunner::runAllTests()
static OscillatorTest oscillator_test;