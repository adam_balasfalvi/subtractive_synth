/*
  ==============================================================================

    SVFilter.h
    Created: 12 Aug 2021 5:00:16pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class SVFilter
{
public:
    enum class FilterType { LowPass, BandPass, HighPass };
    const juce::String FilterTypeToString(FilterType f) noexcept;

    void prepare(const float sampleRate);
    void reset();
    void setFilterType(const int choice);
    void process(juce::dsp::AudioBlock<float>& block);
    void setFilter(const float cutoff, const float q, const float modulation = 1.0f);

    FilterType getFilterType() const;
    juce::String getFilterTypeString();
    float getSampleRate() const;
    float getCutoff() const;
    float getQ() const;
    const float& getZ1(int i) const;
    const float& getZ2(int i) const;

private:
    void calculateParams();

    FilterType type;

    float sampleRate = 0.0f;
    float cutoff = 0.0f;
    float q = 0.5f;

    float g = 1.0f;
    float R = 1.0f;

    float z1[2] = { 0.0f };
    float z2[2] = { 0.0f };
};
