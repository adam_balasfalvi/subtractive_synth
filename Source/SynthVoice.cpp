/*
  ==============================================================================

    SynthVoice.cpp
    Created: 23 Mar 2021 5:43:06pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#include "SynthVoice.h"

/// <summary>
/// The default empty constructor.
/// </summary>
SynthVoice::SynthVoice()
{

}

/// <summary>
/// Return whether the synth object is created or not.
/// </summary>
/// <param name="sound">The sound object of the synthesizer.</param>
/// <returns>A boolean.</returns>
bool SynthVoice::canPlaySound (juce::SynthesiserSound* sound)
{
    return dynamic_cast<juce::SynthesiserSound*>(sound) != nullptr;
}

/// <summary>
/// Sets the oscillator frequency and starts the ADSR and modulation envelope after a key is pressed.
/// </summary>
/// <param name="midiNoteNumber">The pressed key's MIDI note number.</param>
/// <param name="velocity">The velocity of the pressed key.</param>
/// <param name="sound">The sound object of the synthesizer.</param>
/// <param name="currentPitchWheelPosition">The current position of the pitch wheel.</param>
void SynthVoice::startNote (int midiNoteNumber, float velocity, juce::SynthesiserSound *sound, int currentPitchWheelPosition)
{
    oscillator.setWaveFrequency (midiNoteNumber);
    adsr.noteOn();
    modEnv.noteOn();
}

/// <summary>
/// Stops the ADSR and modulation envelope and resets the current voice's state.
/// </summary>
/// <param name="velocity">The velocity of the released key.</param>
/// <param name="allowTailOff">A boolean representing the tail off.</param>
void SynthVoice::stopNote (float velocity, bool allowTailOff)
{
    adsr.noteOff();
    modEnv.noteOff();
    
    if (! allowTailOff || ! adsr.isActive())
        clearCurrentNote();
}

/// <summary>
/// If the controller is moved, this method is called.
/// </summary>
/// <param name="controllerNumber">The index of the moved controller.</param>
/// <param name="newControllerValue">The controller value.</param>
void SynthVoice::controllerMoved (int controllerNumber, int newControllerValue)
{
    
}

/// <summary>
/// If the pitch wheel is moved, this method is called.
/// </summary>
/// <param name="newPitchWheelValue">The pitch wheel value.</param>
void SynthVoice::pitchWheelMoved (int newPitchWheelValue)
{
    
}

/// <summary>
/// Sets the oscillator, ADSR and modulation envelope and filter sample rate. After it is finished, the isPrepared boolean is set to true.
/// </summary>
/// <param name="sampleRate"></param>
/// <param name="samplesPerBlock"></param>
/// <param name="outputChannels"></param>
void SynthVoice::prepareToPlay (double sampleRate, int samplesPerBlock, int outputChannels)
{
    adsr.setSampleRate (sampleRate);
    modEnv.setSampleRate(sampleRate);
    
    juce::dsp::ProcessSpec spec;
    spec.maximumBlockSize = samplesPerBlock;
    spec.sampleRate = sampleRate;
    spec.numChannels = outputChannels;
    
    oscillator.prepareToPlay (spec);  
    filter.prepare(sampleRate);
    
    isPrepared = true;
}

/// <summary>
/// Updates the ADSR envelope's parameters.
/// </summary>
/// <param name="attack">The desired attack rate.</param>
/// <param name="decay">The desired decay rate.</param>
/// <param name="sustain">The desired sustain rate.</param>
/// <param name="release">The desired release rate.</param>
void SynthVoice::updateADSR (const float attack, const float decay, const float sustain, const float release)
{
    adsr.setParameters (attack, decay, sustain, release);
}

/// <summary>
/// Updates the modulation envelope's parameters.
/// </summary>
/// <param name="attack">The desired attack rate.</param>
/// <param name="decay">The desired decay rate.</param>
/// <param name="sustain">The desired sustain rate.</param>
/// <param name="release">The desired release rate.</param>
void SynthVoice::updateModEnv(const float attack, const float decay, const float sustain, const float release)
{
    modEnv.setParameters(attack, decay, sustain, release);
}

/// <summary>
/// Updates the filter's parameters.
/// </summary>
/// <param name="filterType">The desired filter type.</param>
/// <param name="cutoff">The desired cutoff rate.</param>
/// <param name="q">The desired q rate.</param>
void SynthVoice::updateFilter(const int filterType, const float cutoff, const float q)
{
    filter.setFilterType(filterType);
    float modulation = modEnv.process();
    filter.setFilter(cutoff, q, modulation);
}

/// <summary>
/// Renders a block of audio sample. Creates a new buffer, sets its size, then pass it oscillator, filter and envelope process method. After the block is finished, it is copied to the output buffer.
/// </summary>
/// <param name="outputBuffer">The output audio buffer.</param>
/// <param name="startSample">The index of the first sample.</param>
/// <param name="numSamples">The number of samples.</param>
void SynthVoice::renderNextBlock (juce::AudioBuffer<float> &outputBuffer, int startSample, int numSamples)
{
    // ensure that the prepareToPlay() function has completed
    jassert (isPrepared);
    
    if (! isVoiceActive())
        return;
    
    // set the synthBuffer size
    synthBuffer.setSize (outputBuffer.getNumChannels(), numSamples, false, false, true);
    modEnv.applyToBuffer(synthBuffer, 0, numSamples);
    synthBuffer.clear();

    juce::dsp::AudioBlock<float> audioBlock { synthBuffer };

    oscillator.processBlock (audioBlock);
    filter.process(audioBlock);
    adsr.applyToBuffer(synthBuffer, 0, synthBuffer.getNumSamples());
    
    for (int channel = 0; channel < outputBuffer.getNumChannels(); ++channel)
    {
        outputBuffer.addFrom (channel, startSample, synthBuffer, channel, 0, numSamples);
        
        if (! adsr.isActive())
            clearCurrentNote();
    }
}