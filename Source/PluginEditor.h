/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"

//==============================================================================
/**
*/
class Subtractive_synthAudioProcessorEditor  : public juce::AudioProcessorEditor
{
public:
    Subtractive_synthAudioProcessorEditor (Subtractive_synthAudioProcessor&);
    ~Subtractive_synthAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;

private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    Subtractive_synthAudioProcessor& audioProcessor;

	int minWidth;
	int maxWidth;
	int defaultWidth;
	int minHeight;
	int maxHeight;
	int defaultHeight;

	juce::AudioProcessorValueTreeState& valueTreeState;

	juce::StringArray oscillatorChoices{ "Sine", "Sawtooth", "Square", "Triangle" };
	juce::StringArray filterChoices{ "Low-pass", "High-pass", "Band-pass" };
	juce::StringArray adsrChoices{ "Attack", "Decay", "Sustain", "Release" };

	juce::Label oscillatorText;
	juce::Label oscillatorTypeLabel;
	juce::ComboBox oscillatorTypeMenu;
	std::unique_ptr<juce::AudioProcessorValueTreeState::ComboBoxAttachment> oscillatorTypeMenuAttachment;
	juce::Label oscillatorFreqLabel;
	juce::Slider oscillatorFreqSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> oscillatorFreqSliderAttachment;
	juce::Label lfoButtonLabel;
	juce::ToggleButton lfoButton;
	std::unique_ptr<juce::AudioProcessorValueTreeState::ButtonAttachment> lfoButtonAttachment;

	juce::Label lfoText;
	juce::Label lfoFreqLabel;
	juce::Slider lfoFreqSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> lfoFreqSliderAttachment;
	juce::Label lfoAmplitudeLabel;
	juce::Slider lfoAmplitudeSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> lfoAmplitudeSliderAttachment;

	juce::Label filterText;
	juce::Label filterTypeLabel;
	juce::ComboBox filterTypeMenu;
	std::unique_ptr<juce::AudioProcessorValueTreeState::ComboBoxAttachment> filterTypeMenuAttachment;
	juce::Label filterCutoffLabel;
	juce::Slider filterCutoffSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> filterCutoffSliderAttachment;
	juce::Label filterQLabel;
	juce::Slider filterQSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> filterQSliderAttachment;

	juce::Label modText;
	juce::Label modAttackLabel;
	juce::Slider modAttackSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> modAttackSliderAttachment;
	juce::Label modDecayLabel;
	juce::Slider modDecaySlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> modDecaySliderAttachment;
	juce::Label modSustainLabel;
	juce::Slider modSustainSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> modSustainSliderAttachment;
	juce::Label modReleaseLabel;
	juce::Slider modReleaseSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> modReleaseSliderAttachment;

	juce::Label adsrText;
	juce::Label adsrAttackLabel;
	juce::Slider adsrAttackSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> adsrAttackSliderAttachment;
	juce::Label adsrDecayLabel;
	juce::Slider adsrDecaySlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> adsrDecaySliderAttachment;
	juce::Label adsrSustainLabel;
	juce::Slider adsrSustainSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> adsrSustainSliderAttachment;
	juce::Label adsrReleaseLabel;
	juce::Slider adsrReleaseSlider;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> adsrReleaseSliderAttachment;


    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (Subtractive_synthAudioProcessorEditor)
};
