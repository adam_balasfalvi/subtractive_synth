/*
  ==============================================================================

    Oscillator.h
    Created: 23 Mar 2021 5:43:22pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "LFO.h"

class Oscillator : public juce::dsp::Oscillator<float>
{
public:
    void prepareToPlay (juce::dsp::ProcessSpec& spec);
    void setWaveType (const int choice);
    void setWaveFrequency (const int midiNoteNumber);
    void setLFO(const float freq, const float amplitude);
    void setModulation(const float modulation);
    void processBlock (juce::dsp::AudioBlock<float>& block);
    void resetState();

    float phaseShift(const float x, const float shift);
    float freqDouble(const float x);

    float sineWave(const float x);
    float sawWaveTrivial(const float x);
    float sawWaveDPW_0(const float x);
    float sawWaveDPW_1(const float x);
    float squareWaveTrivial(const float x);
    float squareWaveDPW(const float x);
    float triangleWaveTrivial(const float x);
    float triangleWaveDPW(const float x);

    float getSampleRate() const;
    float getFreq() const;
    float getModulation() const;
    float getC() const;

private:
    juce::dsp::LookupTableTransform<float> sineLut;

    float sampleRate;
    float freq;
    LFO lfo;
    float modulation = 0.0f;
    int lastMidiNote = 0;

    float c = 0.0f;
};