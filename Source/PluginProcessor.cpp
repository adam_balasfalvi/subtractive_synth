/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

#include "ADSRTest.h"
#include "LFOTest.h"
#include "OscillatorTest.h"
#include "SVFilterTest.h"

//==============================================================================
/// <summary>
/// Constructor of the audio processor. It initialises the value tree state and also adds a sound and its voices to the synth.
/// </summary>
Subtractive_synthAudioProcessor::Subtractive_synthAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
	: AudioProcessor(BusesProperties()
		#if ! JucePlugin_IsMidiEffect
			#if ! JucePlugin_IsSynth
				.withInput("Input", juce::AudioChannelSet::stereo(), true)
			#endif
				.withOutput("Output", juce::AudioChannelSet::stereo(), true)
		#endif
	), valueTreeState(*this, nullptr, "Parameters", createParameters())
#endif
{
	// Tests
	#ifdef _DEBUG
		juce::Array<juce::UnitTest*> allTests = juce::UnitTest::getAllTests();
		unitTestRunner.runAllTests();
	#endif // _DEBUG

	// Creating the sound
	synth.addSound(new SynthSound());

	// Creating the voices
	for (int i = 0; i < VOICE_NUM; i++)
	{
		synth.addVoice(new SynthVoice());
	}
}

/// <summary>
/// Destructor of the audio processor.
/// </summary>
Subtractive_synthAudioProcessor::~Subtractive_synthAudioProcessor()
{
}

//==============================================================================
/// <summary>
/// Returns the plugin name.
/// </summary>
/// <returns>The plugin name.</returns>
const juce::String Subtractive_synthAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

/// <summary>
/// Indicates whether the plugin accepts MIDI or not.
/// </summary>
/// <returns>A boolean.</returns>
bool Subtractive_synthAudioProcessor::acceptsMidi() const
{
	#if JucePlugin_WantsMidiInput
		return true;
	#else
		return false;
	#endif
}

/// <summary>
/// Indicates whether the plugin produces MIDI signal or not.
/// </summary>
/// <returns>A boolean.</returns>
bool Subtractive_synthAudioProcessor::producesMidi() const
{
	#if JucePlugin_ProducesMidiOutput
		return true;
	#else
		return false;
	#endif
}

/// <summary>
/// Indicates if the plugin is an effect or not.
/// </summary>
/// <returns>A boolean.</returns>
bool Subtractive_synthAudioProcessor::isMidiEffect() const
{
	#if JucePlugin_IsMidiEffect
		return true;
	#else
		return false;
	#endif
}

/// <summary>
/// Returns the plugin's tail length.
/// </summary>
/// <returns>The tail length.</returns>
double Subtractive_synthAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

/// <summary>
/// Returns the number of programs in the plugin.
/// </summary>
/// <returns>The number of programs.</returns>
int Subtractive_synthAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

/// <summary>
/// Returns the index of the current program. 
/// </summary>
/// <returns>The index of the current program.</returns>
int Subtractive_synthAudioProcessor::getCurrentProgram()
{
    return 0;
}

/// <summary>
/// Sets the program of the plugin.
/// </summary>
/// <param name="index">The index of the program.</param>
void Subtractive_synthAudioProcessor::setCurrentProgram (int index)
{
}

/// <summary>
/// Returns the desired program name.
/// </summary>
/// <param name="index">The index of the program.</param>
/// <returns>The program name.</returns>
const juce::String Subtractive_synthAudioProcessor::getProgramName (int index)
{
    return {};
}

/// <summary>
/// Changes the given program name.
/// </summary>
/// <param name="index">The index of the program.</param>
/// <param name="newName">The new name of the program.</param>
void Subtractive_synthAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
/// <summary>
/// Sets the plugin's sample rate and sets the number of samples in a block. Also calls the individual voice's own prepareToPlay method.
/// </summary>
/// <param name="sampleRate">The plugin's sample rate.</param>
/// <param name="samplesPerBlock">The plugin's block size.</param>
void Subtractive_synthAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
    // Use this method as the place to do any pre-playback
    // initialisation that you need..

	synth.setCurrentPlaybackSampleRate(sampleRate);

	for (int i = 0; i < synth.getNumVoices(); i++)
	{
		if (auto voice = dynamic_cast<SynthVoice*>(synth.getVoice(i)))
		{
			voice->prepareToPlay(sampleRate, samplesPerBlock, getTotalNumOutputChannels());
		}
	}
}

/// <summary>
/// Relases the resources after playing.
/// </summary>
void Subtractive_synthAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

/// <summary>
/// Returns whether the current bus layout is supported or not.
/// </summary>
#ifndef JucePlugin_PreferredChannelConfigurations
bool Subtractive_synthAudioProcessor::isBusesLayoutSupported(const BusesLayout& layouts) const
{
	#if JucePlugin_IsMidiEffect
		juce::ignoreUnused(layouts);
		return true;
	#else
		// This is the place where you check if the layout is supported.
		// In this template code we only support mono or stereo.
		if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
			&& layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
			return false;

		// This checks if the input layout matches the output layout
	#if ! JucePlugin_IsSynth
		if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
			return false;
	#endif

		return true;
	#endif
}
#endif

/// <summary>
/// Produces a block of audio output. It also gets the current values from the value tree state.
/// </summary>
/// <param name="buffer">The audio buffer.</param>
/// <param name="midiMessages">The current MIDI message.</param>
void Subtractive_synthAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.

    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

	for (int i = 0; i < synth.getNumVoices(); ++i)
	{
		if (auto voice = dynamic_cast<SynthVoice*>(synth.getVoice(i)))
		{
			auto& oscWaveChoice = *valueTreeState.getRawParameterValue("OSCTYPE");

			auto& lfoFreq = *valueTreeState.getRawParameterValue("LFOFREQ");
			auto& lfoAmplitude = *valueTreeState.getRawParameterValue("LFOAMPLITUDE");

			auto& attack = *valueTreeState.getRawParameterValue("ATTACK");
			auto& decay = *valueTreeState.getRawParameterValue("DECAY");
			auto& sustain = *valueTreeState.getRawParameterValue("SUSTAIN");
			auto& release = *valueTreeState.getRawParameterValue("RELEASE");

			auto& filterType = *valueTreeState.getRawParameterValue("FILTERTYPE");
			auto& filterCutoff = *valueTreeState.getRawParameterValue("FILTERCUTOFF");
			auto& filterQ = *valueTreeState.getRawParameterValue("FILTERQ");

			auto& modAttack = *valueTreeState.getRawParameterValue("MODATTACK");
			auto& modDecay = *valueTreeState.getRawParameterValue("MODDECAY");
			auto& modSustain = *valueTreeState.getRawParameterValue("MODSUSTAIN");
			auto& modRelease = *valueTreeState.getRawParameterValue("MODRELEASE");

			voice->getOscillator().setWaveType(oscWaveChoice.load());
			voice->getOscillator().setLFO(lfoFreq.load(), lfoAmplitude.load());
			voice->updateModEnv(modAttack.load(), modDecay.load(), modSustain.load(), modRelease.load());
			voice->updateFilter(filterType.load(), filterCutoff.load(), filterQ.load());
			voice->updateADSR(attack.load(), decay.load(), sustain.load(), release.load());
		}
	}

	synth.renderNextBlock(buffer, midiMessages, 0, buffer.getNumSamples());
}

//==============================================================================
/// <summary>
/// Returns whether the plugin has GUI or not.
/// </summary>
/// <returns>A boolean.</returns>
bool Subtractive_synthAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

/// <summary>
/// Creates a plugin GUI instance.
/// </summary>
/// <returns>A plugin GUI instance.</returns>
juce::AudioProcessorEditor* Subtractive_synthAudioProcessor::createEditor()
{
    return new Subtractive_synthAudioProcessorEditor (*this);
}

//==============================================================================
/// <summary>
/// Stores the parameters of the plugin.
/// </summary>
/// <param name="destData">The destination of the saving.</param>
void Subtractive_synthAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
    // You should use this method to store your parameters in the memory block.
    // You could do that either as raw data, or use the XML or ValueTree classes
    // as intermediaries to make it easy to save and load complex data.
}

/// <summary>
/// Gets the saved parameters of the plugin.
/// </summary>
/// <param name="data">The saved data.</param>
/// <param name="sizeInBytes">The size of the saved data.</param>
void Subtractive_synthAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
    // You should use this method to restore your parameters from this memory block,
    // whose contents will have been created by the getStateInformation() call.
}

//==============================================================================
/// <summary>
/// Creates and initialises the parameter types as a vector.
/// </summary>
/// <returns>The vector containing the parameters.</returns>
juce::AudioProcessorValueTreeState::ParameterLayout Subtractive_synthAudioProcessor::createParameters()
{
	std::vector<std::unique_ptr<juce::RangedAudioParameter>> params;

	// Oscillator
	params.push_back(std::make_unique<juce::AudioParameterChoice>("OSCTYPE", "Oscillator Type", juce::StringArray{ "Sine", "Sawtooth", "Square", "Triangle" }, 0));

	// LFO
	params.push_back(std::make_unique<juce::AudioParameterFloat>("LFOFREQ", "LFO Frequency", juce::NormalisableRange<float> { 0.0f, 20000.0f, 0.01f, 0.3f }, 10.0f));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("LFOAMPLITUDE", "LFO Amplitude", juce::NormalisableRange<float> { 0.0f, 1000.0f, 0.01f }, 500.0f));

	// Filter
	params.push_back(std::make_unique<juce::AudioParameterChoice>("FILTERTYPE", "Filter Type", juce::StringArray{ "Low-pass", "Band-pass", "High-pass" }, 0));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("FILTERCUTOFF", "Filter Cutoff Frequency", juce::NormalisableRange<float> { 0.0f, 20000.0f, 0.01f, 0.3f }, 4000.0f));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("FILTERQ", "Filter Q Frequency", juce::NormalisableRange<float> { 0.025f, 40.0f, 0.01f }, 0.5f));

	// ADSR
	params.push_back(std::make_unique<juce::AudioParameterFloat>("ATTACK", "Attack", juce::NormalisableRange<float> { 0.01f, 1.0f, 0.01f }, 8.0f));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("DECAY", "Decay", juce::NormalisableRange<float> { 0.01f, 1.0f, 0.01f }, 8.0f));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("SUSTAIN", "Sustain", juce::NormalisableRange<float> { 0.01f, 1.0f, 0.01f }, 8.0f));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("RELEASE", "Release", juce::NormalisableRange<float> { 0.01f, 1.0f, 0.01f }, 8.0f));

	// Modulation Envelope
	params.push_back(std::make_unique<juce::AudioParameterFloat>("MODATTACK", "Attack", juce::NormalisableRange<float> { 0.01f, 1.0f, 0.01f }, 8.0f));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("MODDECAY", "Decay", juce::NormalisableRange<float> { 0.01f, 1.0f, 0.01f }, 8.0f));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("MODSUSTAIN", "Sustain", juce::NormalisableRange<float> { 0.01f, 1.0f, 0.01f }, 8.0f));
	params.push_back(std::make_unique<juce::AudioParameterFloat>("MODRELEASE", "Release", juce::NormalisableRange<float> { 0.01f, 1.0f, 0.01f }, 8.0f));

	return { params.begin(), params.end() };
}

//==============================================================================
/// <summary>
/// Creates new instances of the plugin.
/// </summary>
/// <returns>A new plugin instance.</returns>
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new Subtractive_synthAudioProcessor();
}

