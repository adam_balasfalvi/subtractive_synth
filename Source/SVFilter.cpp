/*
  ==============================================================================

    SVFilter.cpp
    Created: 12 Aug 2021 5:00:16pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#include "SVFilter.h"

/// <summary>
/// Returns the FilterType enum values in string format.
/// </summary>
/// <param name="s">The FilterType enum value.</param>
/// <returns>The filter type name in string format.</returns>
const juce::String SVFilter::FilterTypeToString(FilterType f) noexcept
{
    switch (f)
    {
    case SVFilter::FilterType::LowPass:
        return "LowPass";
        break;
    case SVFilter::FilterType::BandPass:
        return "BandPass";
        break;
    case SVFilter::FilterType::HighPass:
        return "HighPass";
        break;
    }
}

/// <summary>
/// Sets the filter with the desired sample rate and initialises the default filter type as a low pass filter.
/// </summary>
/// <param name="sampleRate">The desired sample rate.</param>
void SVFilter::prepare(const float sampleRate)
{
    this->sampleRate = sampleRate;

    type = FilterType::LowPass;
}

/// <summary>
/// Resets the filter state.
/// </summary>
void SVFilter::reset()
{
    memset(z1, 0.0f, sizeof(float) * 2);
    memset(z2, 0.0f, sizeof(float) * 2);
}

/// <summary>
/// Sets the desired filter type.
/// </summary>
/// <param name="choice">The index of the filter type.</param>
void SVFilter::setFilterType(const int choice)
{
    switch (choice)
    {
    case 0:
        type = FilterType::LowPass;
        break;
    case 1:
        type = FilterType::HighPass;
        break;
    case 2:
        type = FilterType::BandPass;
        break;
    default:
        jassertfalse;
        break;
    }
}

/// <summary>
/// Processes a block of audio sample. It calculates the three types of filtered samples.
/// </summary>
/// <param name="block">The processed audio block.</param>
void SVFilter::process(juce::dsp::AudioBlock<float>& block)
{
    float input = 0;

    for (int sample = 0; sample < block.getNumSamples(); sample++)
    {
        for (int channel = 0; channel < block.getNumChannels(); channel++)
        {
            input = block.getSample(channel, sample);

            const float highPass =
                (input - (2.0f * R + g) * z1[channel] - z2[channel]) /
                (1.0f + (2.0f * R * g) + g * g);

            const float bandPass = highPass * g + z1[channel];

            const float lowPass = bandPass * g + z2[channel];

            z1[channel] = g * highPass + bandPass;
            z2[channel] = g * bandPass + lowPass;

            switch (type)
            {
            case FilterType::LowPass:
                block.setSample(channel, sample, lowPass);
                break;
            case FilterType::HighPass:
                block.setSample(channel, sample, highPass);
                break;
            case FilterType::BandPass:
                block.setSample(channel, sample, bandPass);
                break;
            default:
                jassertfalse;
                break;
            }
        }
    }
}

/// <summary>
/// Sets the filter's cutoff then calls the method responsible for refreshing the filter parameters.
/// </summary>
/// <param name="cutoff">The desired cutoff rate.</param>
/// <param name="q">The desired q rate.</param>
/// <param name="modulation">The desired modulation rate.</param>
void SVFilter::setFilter(const float cutoff, const float q, const float modulation)
{
    float modCutoff = cutoff * modulation;
    if (modulation == 0.0f)
    {
        modCutoff = cutoff;
    }
   
    modCutoff = std::fmax(modCutoff, 20.0f);
    modCutoff = std::fmin(modCutoff, 20000.0f);

    this->cutoff = modCutoff;
    this->q = q;

    calculateParams();
}

/// <summary>
/// Gets the filter's actual type.
/// </summary>
/// <returns>The filter's type.</returns>
SVFilter::FilterType SVFilter::getFilterType() const
{
    return SVFilter::type;
}

/// <summary>
/// Gets the filter's actual type in string format.
/// </summary>
/// <returns>The filter's type in string format.</returns>
juce::String SVFilter::getFilterTypeString()
{
    return FilterTypeToString(getFilterType());
}

/// <summary>
/// Gets the filter's sample rate.
/// </summary>
/// <returns>The filter's sample rate.</returns>
float SVFilter::getSampleRate() const
{
    return SVFilter::sampleRate;
}

/// <summary>
/// Gets the filter's actual cutoff rate.
/// </summary>
/// <returns>The actual cutoff rate.</returns>
float SVFilter::getCutoff() const
{
    return SVFilter::cutoff;
}

/// <summary>
/// Gets the filter's actual Q value.
/// </summary>
/// <returns>The actual Q value.</returns>
float SVFilter::getQ() const
{
    return SVFilter::q;
}

/// <summary>
/// Calculates the filter parameters.
/// </summary>
void SVFilter::calculateParams()
{
    float wd = cutoff * 2.0f * juce::MathConstants<float>::pi;
    float T = 1.0f / sampleRate;
    float wa = (2.0f / T) * juce::dsp::FastMathApproximations::tan(wd * T / 2.0f);

    g = wa * T / 2.0f;
    R = 1.0f / (2.0f * q);
}

/// <summary>
/// Gets the filter's channel one state variables.
/// </summary>
/// <param name="i">Index of state variable.</param>
/// <returns>Channel one state variable located at i.</returns>
const float& SVFilter::getZ1(int i) const
{
    return SVFilter::z1[i];
}

/// <summary>
/// Gets the filter's channel two state variables.
/// </summary>
/// <param name="i">Index of state variable.</param>
/// <returns>Channel two state variable located at i.</returns>
const float& SVFilter::getZ2(int i) const
{
    return SVFilter::z2[i];
}