/*
  ==============================================================================

    SynthVoice.h
    Created: 23 Mar 2021 5:43:06pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "SynthSound.h"
#include "ADSR.h"
#include "Oscillator.h"
#include "SVFilter.h"

class SynthVoice : public juce::SynthesiserVoice
{
public:
    SynthVoice();
    bool canPlaySound (juce::SynthesiserSound* sound) override;
    void startNote (int midiNoteNumber, float velocity, juce::SynthesiserSound *sound, int currentPitchWheelPosition) override;
    void stopNote (float velocity, bool allowTailOff) override;
    void controllerMoved (int controllerNumber, int newControllerValue) override;
    void pitchWheelMoved (int newPitchWheelValue) override;
    void prepareToPlay (double sampleRate, int samplesPerBlock, int outputChannels);
    void renderNextBlock (juce::AudioBuffer< float > &outputBuffer, int startSample, int numSamples) override;
    void updateADSR(const float attack, const float decay, const float sustain, const float release);
    void updateModEnv(const float attack, const float decay, const float sustain, const float release);
    void updateFilter(const int filterType, const float cutoff, const float q);
    Oscillator& getOscillator() { return oscillator; }
    SVFilter& getFilter() { return filter; }
    
private:
    juce::AudioBuffer<float> synthBuffer;
    
    ADSR adsr;
    ADSR modEnv;
    Oscillator oscillator;
    SVFilter filter;

    bool isPrepared{ false };
};