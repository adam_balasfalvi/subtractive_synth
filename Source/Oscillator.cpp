/*
  ==============================================================================

    Oscillator.cpp
    Created: 23 Mar 2021 5:43:22pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#include "Oscillator.h"

/// <summary>
/// Prepares the oscillator with the desired specifiations and also populates the sine lookup table with values.
/// </summary>
/// <param name="spec">The specifications of the oscillator. It contains the maximum block size, number of channels and sample rate.</param>
void Oscillator::prepareToPlay (juce::dsp::ProcessSpec& spec)
{
    prepare (spec);
    lfo.prepareToPlay(spec);
    sampleRate = spec.sampleRate;

    sineLut.initialise([](float x) {return sin(x); }, -juce::MathConstants<float>::pi, juce::MathConstants<float>::pi, 256);
}

/// <summary>
/// Sets the oscillator with the choosen waveform.
/// </summary>
/// <param name="choice">The number of the choosen waveform.</param>
void Oscillator::setWaveType (const int choice)
{
    switch (choice)
    {
        case 0:
            // Sine wave
            initialise ([this](float x) {  
                return sineWave(x);
            });
            break;
            
        case 1:
            // Saw wave
            initialise ([this](float x) {  
                return sawWaveTrivial(x);
                //return sawWaveDPW_0(x);
            });
            break;
            
        case 2:
            // Square wave
            initialise([this](float x) { 
                return squareWaveTrivial(x);
                //return squareWaveDPW(x);
            });
            break;

        case 3:
            initialise([this](float x) {
                return triangleWaveTrivial(x);
                //return triangleWaveDPW(x);
            });
            break;

        default:
            jassertfalse;
            break;
    }
}

/// <summary>
/// Sets the oscillator's frequency. Also calculates the c constant needed for the DPW algorithm.
/// </summary>
/// <param name="midiNoteNumber">The currently played MIDI note number.</param>
void Oscillator::setWaveFrequency(const int midiNoteNumber)
{
    freq = juce::MidiMessage::getMidiNoteInHertz(midiNoteNumber) + modulation;
    if (freq < 0)
    {
        freq = freq * -1.0f;
    }
    lastMidiNote = midiNoteNumber;
    setFrequency(freq, true);

    c = sampleRate / (4.0f * freq * (1.0f - freq / sampleRate));
}

/// <summary>
/// Sets the LFO's frequency and amplitude then calls the oscillator own method to set the new frequency.
/// </summary>
/// <param name="freq">The desired frequency.</param>
/// <param name="amplitude">The desired amplitude.</param>
void Oscillator::setLFO(const float freq, const float amplitude)
{
    lfo.setWave(freq, amplitude);
    setWaveFrequency(lastMidiNote);
}

void Oscillator::setModulation(const float modulation)
{
    Oscillator::modulation = modulation;
}

/// <summary>
/// Process a block of audio sample. Also sets the modulation value and with the new modulation value sets the frequency of the oscillator.
/// </summary>
/// <param name="block">The processed audio block.</param>
void Oscillator::processBlock (juce::dsp::AudioBlock<float>& block)
{
    modulation = lfo.calculateModulation();
    setWaveFrequency(lastMidiNote);
    process(juce::dsp::ProcessContextReplacing<float>(block));
}

/// <summary>
/// Resets the oscillator's and LFO's state.
/// </summary>
void Oscillator::resetState()
{
    Oscillator::freq = 0.0f;
    Oscillator::modulation = 0.0f;
    Oscillator::c = 0.0f;
    reset();
    lfo.reset();
}

/// <summary>
/// Shifts the phase with the given value. The x value must be between -pi and +pi.
/// </summary>
/// <param name="x">The original phase.</param>
/// <param name="shift">The value of the shift.</param>
/// <returns>The shifted phase.</returns>
float Oscillator::phaseShift(float x, const float shift)
{
    if (x + shift > juce::MathConstants<float>::pi)
    {
        return -juce::MathConstants<float>::pi + x;
    }
    if (x + shift < -juce::MathConstants<float>::pi)
    {
        return juce::MathConstants<float>::pi + x;
    }
    return x + shift;
}

/// <summary>
/// Doubles the frequency. The x value must be between -pi and +pi.
/// </summary>
/// <param name="x">The original phase.</param>
/// <returns>The modified phase.</returns>
float Oscillator::freqDouble(const float x)
{
    float halfPhase = juce::jmap(x, -juce::MathConstants<float>::pi, juce::MathConstants<float>::pi, -juce::MathConstants<float>::twoPi, juce::MathConstants<float>::twoPi);
    if (x < 0.0f)
    {
        halfPhase += juce::MathConstants<float>::pi;
    }
    else
    {
        halfPhase -= juce::MathConstants<float>::pi;
    }
    return halfPhase;
}

/// <summary>
/// Creates a sine wave from a lookup table.
/// </summary>
/// <param name="x">Phase of the signal.</param>
/// <returns>Value of the sine at the given phase.</returns>
float Oscillator::sineWave(const float x)
{
    return sineLut(x);
}

/// <summary>
/// Creates a saw wave with a trivial method.
/// </summary>
/// <param name="x">Phase of the signal.</param>
/// <returns>Value of the saw wave at the given phase.</returns>
float Oscillator::sawWaveTrivial(const float x)
{
    return x / juce::MathConstants<float>::pi;
}

/// <summary>
/// Creates a saw wave with the DPW method.
/// </summary>
/// <param name="x">Phase of the signal.</param>
/// <returns>Value of the saw wave at the given phase.</returns>
float Oscillator::sawWaveDPW_0(const float x)
{
    static float state = 0.0f;

    float count = x / juce::MathConstants<float>::pi;
    float sqr = juce::square(count);
    float diff = sqr - state;
    state = sqr;
    float y = diff * c;
    return y;
}

/// <summary>
/// Creates a saw wave with the DPW method.
/// </summary>
/// <param name="x">Phase of the signal.</param>
/// <returns>Value of the saw wave at the given phase.</returns>
float Oscillator::sawWaveDPW_1(const float x)
{
    static float state = 0.0f;

    float count = x / juce::MathConstants<float>::pi;
    float sqr = juce::square(count);
    float diff = sqr - state;
    state = sqr;
    float y = diff * c;
    return y;
}

/// <summary>
/// Creates a square wave with a trivial method.
/// </summary>
/// <param name="x">Phase of the signal.</param>
/// <returns>Value of the square wave at the given phase.</returns>
float Oscillator::squareWaveTrivial(const float x)
{
    if (x < 0.0f)
    {
        return -1.0f;
    }
    else
    {
        return 1.0f;
    }
}

/// <summary>
/// Creates a square wave with the DPW method.
/// </summary>
/// <param name="x">Phase of the signal.</param>
/// <returns>Value of the square wave at the given phase.</returns>
float Oscillator::squareWaveDPW(const float x)
{
    float shiftedX = phaseShift(x, juce::MathConstants<float>::pi);
    float saw = sawWaveDPW_0(x);
    float shiftedSaw = -(sawWaveDPW_1(shiftedX));
    float square = saw + shiftedSaw;
    return square;
}

/// <summary>
/// Creates a triangle wave with a trivial method.
/// </summary>
/// <param name="x">Phase of the signal.</param>
/// <returns>Value of the triangle wave at the given phase.</returns>
float Oscillator::triangleWaveTrivial(const float x)
{
    float saw = x / juce::MathConstants<float>::pi;
    float triangle = 2 * (((-1) * std::abs(saw)) + 0.5f);
    return triangle;
}

/// <summary>
/// Creates a triangle wave with the DPW method.
/// </summary>
/// <param name="x">Phase of the signal.</param>
/// <returns>Value of the triangle wave at the given phase.</returns>
float Oscillator::triangleWaveDPW(const float x)
{
    static float state = 0.0f;

    float doublePhase = freqDouble(x);

    float count = doublePhase / juce::MathConstants<float>::pi;
    float sqr = juce::square(count);
    float invSqr = 1 - sqr;
    float square = squareWaveTrivial(x);
    float modSqr = invSqr * square;
    float diff = modSqr - state;
    state = modSqr;
    float triangle = diff * c / 2;
    return triangle;
}

/// <summary>
/// Gets the oscillator's sample rate;
/// </summary>
/// <returns>The sample rate.</returns>
float Oscillator::getSampleRate() const
{
    return Oscillator::sampleRate;
}


/// <summary>
/// Gets the oscillator's frequency.
/// </summary>
/// <returns>The actual frequency.</returns>
float Oscillator::getFreq() const
{
    return Oscillator::freq;
}

/// <summary>
/// Gets the oscillator's actual modulation rate.
/// </summary>
/// <returns>The actual modulation rate.</returns>
float Oscillator::getModulation() const
{
    return Oscillator::modulation;
}

/// <summary>
/// Gets the value of the c constant required for the DPW algorithm.
/// </summary>
/// <returns>The actual value of the c constant.</returns>
float Oscillator::getC() const
{
    return Oscillator::c;
}
