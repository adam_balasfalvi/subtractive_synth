/*
  ==============================================================================

    ADSRTest.h
    Created: 31 Oct 2021 2:20:45pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "ADSR.h"

class ADSRTest : public juce::UnitTest
{
public:
    ADSRTest() : UnitTest("ADSR testing") {}

    void runTest() override
    {
        ADSR adsr;

        beginTest("ADSR Init");
        expectEquals(adsr.getSampleRate(), 44100.0);
        expectEquals(adsr.getStateString(), adsr.StateToString(ADSR::State::Idle));

        beginTest("Reset");
        adsr.reset();
        expectEquals(adsr.getA(), 0.0f);
        expectEquals(adsr.getStateString(), adsr.StateToString(ADSR::State::Idle));

        beginTest("Note on");
        adsr.reset();
        adsr.setParameters(0.5f, 0.0f, 0.0f, 0.0f);
        adsr.noteOn();
        expectEquals(adsr.getStateString(), adsr.StateToString(ADSR::State::Attack));

        adsr.reset();
        adsr.setParameters(0.0f, 0.5f, 0.0f, 0.0f);
        adsr.noteOn();
        expectEquals(adsr.getStateString(), adsr.StateToString(ADSR::State::Decay));
        expectEquals(adsr.getA(), 1.0f);

        beginTest("Note off");
        adsr.reset();
        adsr.setParameters(0.0f, 0.0f, 0.0f, 0.5f);
        adsr.noteOff();
        expectEquals(adsr.getStateString(), adsr.StateToString(ADSR::State::Release));

        beginTest("Process");
        adsr.reset();
        adsr.setParameters(0.5f, 0.5f, 0.5f, 0.5f);
        adsr.setState(ADSR::State::Idle);
        adsr.setA(1.0f);
        adsr.process();
        expectEquals(adsr.getA(), 0.0f);

        adsr.reset();
        adsr.setParameters(0.5f, 0.5f, 0.5f, 0.5f);
        adsr.setState(ADSR::State::Attack);
        adsr.setA(1.0f);
        adsr.process();
        expectEquals(adsr.getA(), 1.0f);

        adsr.reset();
        adsr.setParameters(0.5f, 0.5f, 0.5f, 0.5f);
        adsr.setState(ADSR::State::Sustain);
        adsr.setA(1.0f);
        adsr.process();
        expectEquals(adsr.getA(), 0.5f);

        adsr.reset();
        adsr.setParameters(0.5f, 0.5f, 0.5f, 0.5f);
        adsr.setState(ADSR::State::Release);
        adsr.setA(0.0f);
        adsr.process();
        expectEquals(adsr.getA(), 0.0f);
    }
};

// Creating a static instance will automatically add the instance to the array
// returned by UnitTest::getAllTests(), so the test will be included when you call
// UnitTestRunner::runAllTests()
static ADSRTest adsr_test;