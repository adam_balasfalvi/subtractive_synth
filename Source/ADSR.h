/*
  ==============================================================================

    ADSR.h
    Created: 23 Mar 2021 5:43:56pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class ADSR
{
public:
    enum class State { Idle, Attack, Decay, Sustain, Release };
    const juce::String StateToString(State e) noexcept;

    ADSR();
    void setParameters(const float attack, const float decay, const float sustain, const float release);
    void setSampleRate(const double sampleRate);
    void setState(const ADSR::State state);
    void setA(const float a);
    bool isActive() const;
    void reset();
    void noteOn();
    void noteOff();
    float process();

    void applyToBuffer(juce::AudioSampleBuffer& buffer, int startSample, int numSamples);

    double getSampleRate() const;
    State getState() const;
    juce::String getStateString();
    float getAttack() const;
    float getDecay() const;
    float getSustain() const;
    float getRelease() const;
    float getA() const;

private:
    void checkState();

    State state = State::Idle;

    float attack = 0.1f;
    float decay = 0.1f;
    float sustain = 1.0f;;
    float release = 0.5f;;

    double sampleRate;

    float aMax = 1.0f;
    float a = 0.0f;
};