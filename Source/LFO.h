/*
  ==============================================================================

    LFO.h
    Created: 15 Jul 2021 5:13:52pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class LFO : public juce::dsp::Oscillator<float>
{
public:
    void prepareToPlay (juce::dsp::ProcessSpec& spec);
    void setWave(const float freq, const float amplitude);
    float calculateModulation();
    void reset();

    float getSampleRate() const;
    float getFreq() const;
    float getAmplitude() const;
private:
    void initLFO();

    float sampleRate;
    float freq;
    float amplitude;
};
