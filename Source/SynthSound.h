/*
  ==============================================================================

    SynthSound.h
    Created: 23 Mar 2021 5:42:50pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class SynthSound : public juce::SynthesiserSound
{
public:
    bool appliesToNote (int midiNoteNumber) override { return true; }
    bool appliesToChannel (int midiChannel) override { return true; }  
};