/*
  ==============================================================================

    ADSR.cpp
    Created: 23 Mar 2021 5:43:56pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#include "ADSR.h"

/// <summary>
/// Returns the State enum values in string format.
/// </summary>
/// <param name="s">The state enum value.</param>
/// <returns>The state name in string format.</returns>
const juce::String ADSR::StateToString(State e) noexcept
{
    switch (e)
    {
    case ADSR::State::Idle:
        return "Idle";
    case ADSR::State::Attack:
        return "Attack";
    case ADSR::State::Decay:
        return "Decay";
    case ADSR::State::Sustain:
        return "Sustain";
    case ADSR::State::Release:
        return "Release";
    }
}

/// <summary>
/// Constructor of the ADSR class. Default sample rate is 44.1 kHz.
/// </summary>
ADSR::ADSR()
{
    setSampleRate(44100.0);
}

/// <summary>
/// Setting the parameters of the ADSR as a percentage. Maximum value is 1.
/// </summary>
/// <param name="attack">The desired attack rate.</param>
/// <param name="decay">The desired decay rate.</param>
/// <param name="sustain">The desired sustain rate.</param>
/// <param name="release">The desired release rate.</param>
void ADSR::setParameters(const float attack, const float decay, const float sustain, const float release)
{
    ADSR::attack = attack;
    ADSR::decay = decay;
    ADSR::sustain = sustain;
    ADSR::release = release;
}

/// <summary>
/// Setting the sample rate.
/// </summary>
/// <param name="sampleRate">The desired sample rate.</param>
void ADSR::setSampleRate(const double sampleRate)
{
    jassert (sampleRate > 0.0);
    ADSR::sampleRate = sampleRate;
}

/// <summary>
/// Set the ADSR's state.
/// </summary>
/// <param name="state">The desired state.</param>
void ADSR::setState(const ADSR::State state)
{
    ADSR::state = state;
}

void ADSR::setA(const float a)
{
    ADSR::a = a;
}

/// <summary>
/// Returns whether the envelope is active or not.
/// </summary>
/// <returns>A boolean.</returns>
bool ADSR::isActive() const
{
    return state != State::Idle;
}

/// <summary>
/// Resets the filter.
/// </summary>
void ADSR::reset()
{
    a = 0.0f;
    state = State::Idle;
}

/// <summary>
/// Activates the envelope after a key is pressed.  
/// </summary>
void ADSR::noteOn()
{
    if (attack > 0.0f)
    {
        state = State::Attack;
    }
    else if (decay > 0.0f)
    {
        a = 1.0f;
        state = State::Decay;
    }
    else
    {
        state = State::Sustain;
    }
}

/// <summary>
/// Decides what happens after the key is released.
/// </summary>
void ADSR::noteOff()
{
    if (release > 0.0f)
    {
        state = State::Release;
    }
    else
    {
        reset();
    }
}

/// <summary>
/// Calculates the amplitude depending of the actual state.
/// </summary>
/// <returns>The current amplitude.</returns>
float ADSR::process()
{
    switch (state)
    {
    case ADSR::State::Idle:
        a = 0.0f;
        break;
    case ADSR::State::Attack:
        a += 1 / (sampleRate * attack * aMax);
        if (a > 1.0f)
        {
            a = 1.0f;
        }
        break;
    case ADSR::State::Decay:
        a -= 1 / (sampleRate * decay * aMax);
        if (a < 0.0f)
        {
            a = 0.0f;
        }
        break;
    case ADSR::State::Sustain:
        a = sustain;
        break;
    case ADSR::State::Release:
        a -= 1 / (sampleRate * release * aMax);
        if (a < 0.0f)
        {
            a = 0.0f;
        }
        break;
    default:
        jassertfalse;
        break;
    }
    checkState();
    return a;
}

/// <summary>
/// Applies the calculated amplitude rate to the output buffer.
/// </summary>
/// <param name="buffer">The output buffer.</param>
/// <param name="startSample">The first sample of the buffer.</param>
/// <param name="numSamples">Number of samples in the buffer.</param>
void ADSR::applyToBuffer(juce::AudioSampleBuffer& buffer, int startSample, int numSamples)
{
    jassert(startSample + numSamples <= buffer.getNumSamples());

    auto numChannels = buffer.getNumChannels();

    while (--numSamples >= 0)
    {
        auto env = process();

        for (int i = 0; i < numChannels; ++i)
            buffer.getWritePointer(i)[startSample] *= env;

        ++startSample;
    }
}

/// <summary>
/// Checks which state is the envelope currently in. If the current state is no longer valid, sets the new state.
/// </summary>
void ADSR::checkState()
{
    if (state == State::Attack && a >= 1.0f)
    {
        state = decay > 0.0f ? State::Decay : State::Sustain;
    }
    else if (state == State::Decay && a <= sustain)
    {
        state = State::Sustain;
    }
    else if (state == State::Release && a <= 0.0f)
    {
        reset();
    }
}

/// <summary>
/// Gets the ADSR's sample rate.
/// </summary>
/// <returns>The sample rate.</returns>
double ADSR::getSampleRate() const
{
    return ADSR::sampleRate;
}

/// <summary>
/// Gets the ADSR's state;
/// </summary>
/// <returns>The current state;</returns>
ADSR::State ADSR::getState() const
{
    return ADSR::state;
}
 /// <summary>
 /// Gets the ADSR's state in string format.
 /// </summary>
 /// <returns>The current state in string format.</returns>
juce::String ADSR::getStateString()
{
    return StateToString(getState());
}

/// <summary>
/// Gets the ADSR's attack rate.
/// </summary>
/// <returns>The attack rate.</returns>
float ADSR::getAttack() const
{
    return ADSR::attack;
}

/// <summary>
/// Gets the ADSR's decay rate.
/// </summary>
/// <returns>The decay rate.</returns>
float ADSR::getDecay() const
{
    return ADSR::decay;
}

/// <summary>
/// Gets the ADSR's sustain rate.
/// </summary>
/// <returns>The sustain rate.</returns>
float ADSR::getSustain() const
{
    return ADSR::sustain;
}

/// <summary>
/// Gets the ADSR's release rate.
/// </summary>
/// <returns>The release rate.</returns>
float ADSR::getRelease() const
{
    return ADSR::release;
}

/// <summary>
/// Gets the ADSR's amplitude rate.
/// </summary>
/// <returns>The amplitude rate.</returns>
float ADSR::getA() const
{
    return ADSR::a;
}