/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
/// <summary>
/// This is the constuctor of the plugin's GUI. It also initalises the audio processor and creates the value tree state reference.
/// </summary>
/// <param name="p">The plugin's audio processor.</param>
Subtractive_synthAudioProcessorEditor::Subtractive_synthAudioProcessorEditor (Subtractive_synthAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p), valueTreeState (p.valueTreeState)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
	
	// Setting the parameters of the GUI
	minWidth = 100;
	maxWidth = 800;
	defaultWidth = 650;
	minHeight = 100;
	maxHeight = 800;
	defaultHeight = 400;

	setResizable(true, true);
	setResizeLimits(minWidth, minHeight, maxWidth, maxHeight);
	setSize(defaultWidth, defaultHeight);
	//==============================================================================
	// Every single comment line indicates an element in the plugin GUI. A double comment line means a row of elements.
	//==============================================================================
	addAndMakeVisible(oscillatorText);
	oscillatorText.setFont(juce::Font(16.0f, juce::Font::bold));
	oscillatorText.setText("Oscillator", juce::dontSendNotification);
	oscillatorText.setJustificationType(juce::Justification::left);
	//==============================================================================
	addAndMakeVisible(oscillatorTypeLabel);
	oscillatorTypeLabel.setFont(juce::Font(12.0f));
	oscillatorTypeLabel.setText("Type", juce::dontSendNotification);
	oscillatorTypeLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(oscillatorTypeMenu);
	oscillatorTypeMenu.addItemList(oscillatorChoices, 1);
	oscillatorTypeMenu.setSelectedId(1);
	oscillatorTypeMenuAttachment = std::make_unique<juce::AudioProcessorValueTreeState::ComboBoxAttachment>(valueTreeState, "OSCTYPE", oscillatorTypeMenu);
	//==============================================================================
	//==============================================================================
	addAndMakeVisible(lfoText);
	lfoText.setFont(juce::Font(16.0f, juce::Font::bold));
	lfoText.setText("LFO", juce::dontSendNotification);
	lfoText.setJustificationType(juce::Justification::left);
	//==============================================================================
	addAndMakeVisible(lfoFreqLabel);
	lfoFreqLabel.setFont(juce::Font(12.0f));
	lfoFreqLabel.setText("Freq", juce::dontSendNotification);
	lfoFreqLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(lfoFreqSlider);
	lfoFreqSlider.setTextValueSuffix("Hz");
	lfoFreqSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoFreqSlider.getTextBoxHeight());
	lfoFreqSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "LFOFREQ", lfoFreqSlider);
	//==============================================================================
	addAndMakeVisible(lfoAmplitudeLabel);
	lfoAmplitudeLabel.setFont(juce::Font(12.0f));
	lfoAmplitudeLabel.setText("Amplitude", juce::dontSendNotification);
	lfoAmplitudeLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(lfoAmplitudeSlider);
	lfoAmplitudeSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoAmplitudeSlider.getTextBoxHeight());
	lfoAmplitudeSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "LFOAMPLITUDE", lfoAmplitudeSlider);
	//==============================================================================
	//==============================================================================
	addAndMakeVisible(filterText);
	filterText.setFont(juce::Font(16.0f, juce::Font::bold));
	filterText.setText("Filter", juce::dontSendNotification);
	filterText.setJustificationType(juce::Justification::left);
	//==============================================================================
	addAndMakeVisible(filterTypeLabel);
	filterTypeLabel.setFont(juce::Font(12.0f));
	filterTypeLabel.setText("Type", juce::dontSendNotification);
	filterTypeLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(filterTypeMenu);
	filterTypeMenu.addItemList(filterChoices, 1);
	filterTypeMenu.setSelectedId(1);
	filterTypeMenuAttachment = std::make_unique<juce::AudioProcessorValueTreeState::ComboBoxAttachment>(valueTreeState, "FILTERTYPE", filterTypeMenu);
	//==============================================================================
	addAndMakeVisible(filterCutoffLabel);
	filterCutoffLabel.setFont(juce::Font(12.0f));
	filterCutoffLabel.setText("Cutoff", juce::dontSendNotification);
	filterCutoffLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(filterCutoffSlider);
	filterCutoffSlider.setTextValueSuffix("Hz");
	filterCutoffSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, filterCutoffSlider.getTextBoxHeight());
	filterCutoffSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "FILTERCUTOFF", filterCutoffSlider);
	//==============================================================================
	addAndMakeVisible(filterQLabel);
	filterQLabel.setFont(juce::Font(12.0f));
	filterQLabel.setText("Q", juce::dontSendNotification);
	filterQLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(filterQSlider);
	filterQSlider.setTextValueSuffix("Hz");
	filterQSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, filterQSlider.getTextBoxHeight());
	filterQSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "FILTERQ", filterQSlider);
	//==============================================================================
	//==============================================================================
	addAndMakeVisible(modText);
	modText.setFont(juce::Font(16.0f, juce::Font::bold));
	modText.setText("Mod Envelope", juce::dontSendNotification);
	modText.setJustificationType(juce::Justification::left);
	//==============================================================================
	addAndMakeVisible(modAttackLabel);
	modAttackLabel.setFont(juce::Font(12.0f));
	modAttackLabel.setText("Attack", juce::dontSendNotification);
	modAttackLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(modAttackSlider);
	modAttackSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, modAttackSlider.getTextBoxHeight());
	modAttackSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "MODATTACK", modAttackSlider);
	//==============================================================================
	addAndMakeVisible(modDecayLabel);
	modDecayLabel.setFont(juce::Font(12.0f));
	modDecayLabel.setText("Decay", juce::dontSendNotification);
	modDecayLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(modDecaySlider);
	modDecaySlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, modDecaySlider.getTextBoxHeight());
	modDecaySliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "MODDECAY", modDecaySlider);
	//==============================================================================
	addAndMakeVisible(modSustainLabel);
	modSustainLabel.setFont(juce::Font(12.0f));
	modSustainLabel.setText("Sustain", juce::dontSendNotification);
	modSustainLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(modSustainSlider);
	modSustainSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, modSustainSlider.getTextBoxHeight());
	modSustainSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "MODSUSTAIN", modSustainSlider);
	//==============================================================================
	addAndMakeVisible(modReleaseLabel);
	modReleaseLabel.setFont(juce::Font(12.0f));
	modReleaseLabel.setText("Release", juce::dontSendNotification);
	modReleaseLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(modReleaseSlider);
	modReleaseSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, modReleaseSlider.getTextBoxHeight());
	modReleaseSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "MODRELEASE", modReleaseSlider);
	//==============================================================================
	addAndMakeVisible(adsrDecayLabel);
	adsrDecayLabel.setFont(juce::Font(12.0f));
	adsrDecayLabel.setText("Decay", juce::dontSendNotification);
	adsrDecayLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(adsrDecaySlider);
	adsrDecaySlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoFreqSlider.getTextBoxHeight());
	adsrDecaySliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "DECAY", adsrDecaySlider);
	//==============================================================================
	addAndMakeVisible(adsrSustainLabel);
	adsrSustainLabel.setFont(juce::Font(12.0f));
	adsrSustainLabel.setText("Sustain", juce::dontSendNotification);
	adsrSustainLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(adsrSustainSlider);
	adsrSustainSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoFreqSlider.getTextBoxHeight());
	adsrSustainSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "SUSTAIN", adsrSustainSlider);
	//==============================================================================
	addAndMakeVisible(adsrReleaseLabel);
	adsrReleaseLabel.setFont(juce::Font(12.0f));
	adsrReleaseLabel.setText("Release", juce::dontSendNotification);
	adsrReleaseLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(adsrReleaseSlider);
	adsrReleaseSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoFreqSlider.getTextBoxHeight());
	adsrReleaseSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "RELEASE", adsrReleaseSlider);
	//==============================================================================
	//==============================================================================
	addAndMakeVisible(adsrText);
	adsrText.setFont(juce::Font(16.0f, juce::Font::bold));
	adsrText.setText("ADSR", juce::dontSendNotification);
	adsrText.setJustificationType(juce::Justification::left);
	//==============================================================================
	addAndMakeVisible(adsrAttackLabel);
	adsrAttackLabel.setFont(juce::Font(12.0f));
	adsrAttackLabel.setText("Attack", juce::dontSendNotification);
	adsrAttackLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(adsrAttackSlider);
	adsrAttackSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoFreqSlider.getTextBoxHeight());
	adsrAttackSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "ATTACK", adsrAttackSlider);
	//==============================================================================
	addAndMakeVisible(adsrDecayLabel);
	adsrDecayLabel.setFont(juce::Font(12.0f));
	adsrDecayLabel.setText("Decay", juce::dontSendNotification);
	adsrDecayLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(adsrDecaySlider);
	adsrDecaySlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoFreqSlider.getTextBoxHeight());
	adsrDecaySliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "DECAY", adsrDecaySlider);
	//==============================================================================
	addAndMakeVisible(adsrSustainLabel);
	adsrSustainLabel.setFont(juce::Font(12.0f));
	adsrSustainLabel.setText("Sustain", juce::dontSendNotification);
	adsrSustainLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(adsrSustainSlider);
	adsrSustainSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoFreqSlider.getTextBoxHeight());
	adsrSustainSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "SUSTAIN", adsrSustainSlider);
	//==============================================================================
	addAndMakeVisible(adsrReleaseLabel);
	adsrReleaseLabel.setFont(juce::Font(12.0f));
	adsrReleaseLabel.setText("Release", juce::dontSendNotification);
	adsrReleaseLabel.setJustificationType(juce::Justification::left);

	addAndMakeVisible(adsrReleaseSlider);
	adsrReleaseSlider.setTextBoxStyle(juce::Slider::TextBoxLeft, false, 160, lfoFreqSlider.getTextBoxHeight());
	adsrReleaseSliderAttachment = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment>(valueTreeState, "RELEASE", adsrReleaseSlider);
	//==============================================================================
}

/// <summary>
/// Destructor of the plugin editor.
/// </summary>
Subtractive_synthAudioProcessorEditor::~Subtractive_synthAudioProcessorEditor()
{
}

//===============================================================================
/// <summary>
/// Paints the GUI's background.
/// </summary>
/// <param name="g">A graphics context.</param>
void Subtractive_synthAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
}

/// <summary>
/// This method gets invoked every time the GUI size is changed. The layout follows a matrix pattern: it is made up of grids, and every grid is made up of rows and columns.
/// </summary>
void Subtractive_synthAudioProcessorEditor::resized()
{
    // This is generally where you'll want to lay out the positions of any
    // subcomponents in your editor..

	auto area = getLocalBounds();
	auto minItemWidth = 75;
	auto minItemHeight = 20;
	//==============================================================================
	juce::Grid grid0;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid0.templateRows = { Track(Fr(1)) };
	grid0.templateColumns = { Track(Fr(1)) };

	grid0.items = { juce::GridItem(oscillatorText) };

	grid0.performLayout(area.removeFromTop(minItemHeight));
	//==============================================================================
	juce::Grid grid1;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid1.justifyItems = juce::Grid::JustifyItems::start;

	grid1.templateRows = { Track(Fr(1)) };
	grid1.templateColumns = { Track(Fr(1)), Track(Fr(6))};

	grid1.items = { 
		juce::GridItem(oscillatorTypeLabel), juce::GridItem(oscillatorTypeMenu),
	};

	grid1.performLayout(area.removeFromTop(minItemHeight));
	//==============================================================================
	juce::Grid grid3;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid3.templateRows = { Track(Fr(1)) };
	grid3.templateColumns = { Track(Fr(1)) };

	grid3.items = { juce::GridItem(lfoText) };

	grid3.performLayout(area.removeFromTop(minItemHeight));
	//==============================================================================
	juce::Grid grid4;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid4.justifyItems = juce::Grid::JustifyItems::start;

	grid4.templateRows = { Track(Fr(1)), Track(Fr(1)) };
	grid4.templateColumns = { Track(Fr(1)), Track(Fr(6))};

	grid4.items = { 
		juce::GridItem(lfoFreqLabel), juce::GridItem(lfoFreqSlider),
		juce::GridItem(lfoAmplitudeLabel), juce::GridItem(lfoAmplitudeSlider)
	};

	grid4.performLayout(area.removeFromTop(minItemHeight * 2));
	//==============================================================================
	juce::Grid grid6;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid6.templateRows = { Track(Fr(1)) };
	grid6.templateColumns = { Track(Fr(1)) };

	grid6.items = { juce::GridItem(filterText) };

	grid6.performLayout(area.removeFromTop(minItemHeight));
	//==============================================================================
	juce::Grid grid9;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid9.justifyItems = juce::Grid::JustifyItems::start;

	grid9.templateRows = { Track(Fr(1)), Track(Fr(1)), Track(Fr(1)) };
	grid9.templateColumns = { Track(Fr(1)), Track(Fr(6)) };

	grid9.items = {
		juce::GridItem(filterTypeLabel), juce::GridItem(filterTypeMenu),
		juce::GridItem(filterCutoffLabel), juce::GridItem(filterCutoffSlider),
		juce::GridItem(filterQLabel), juce::GridItem(filterQSlider)
	};

	grid9.performLayout(area.removeFromTop(minItemHeight * 3));
	//==============================================================================
	juce::Grid grid10;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid10.templateRows = { Track(Fr(1)) };
	grid10.templateColumns = { Track(Fr(1)) };

	grid10.items = { juce::GridItem(modText) };

	grid10.performLayout(area.removeFromTop(minItemHeight));
	//==============================================================================
	juce::Grid grid11;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid11.justifyItems = juce::Grid::JustifyItems::start;

	grid11.templateRows = { Track(Fr(1)), Track(Fr(1)), Track(Fr(1)), Track(Fr(1)) };
	grid11.templateColumns = { Track(Fr(1)), Track(Fr(6)) };

	grid11.items = {
		juce::GridItem(modAttackLabel), juce::GridItem(modAttackSlider),
		juce::GridItem(modDecayLabel), juce::GridItem(modDecaySlider),
		juce::GridItem(modSustainLabel), juce::GridItem(modSustainSlider),
		juce::GridItem(modReleaseLabel), juce::GridItem(modReleaseSlider)
	};

	grid11.performLayout(area.removeFromTop(minItemHeight * 4));
	//==============================================================================
	juce::Grid grid12;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid12.templateRows = { Track(Fr(1)) };
	grid12.templateColumns = { Track(Fr(1)) };

	grid12.items = { juce::GridItem(adsrText) };

	grid12.performLayout(area.removeFromTop(minItemHeight));
	//==============================================================================
	juce::Grid grid13;
	using Track = juce::Grid::TrackInfo;
	using Fr = juce::Grid::Fr;

	grid13.justifyItems = juce::Grid::JustifyItems::start;

	grid13.templateRows = { Track(Fr(1)), Track(Fr(1)), Track(Fr(1)), Track(Fr(1)) };
	grid13.templateColumns = { Track(Fr(1)), Track(Fr(6)) };

	grid13.items = {
		juce::GridItem(adsrAttackLabel), juce::GridItem(adsrAttackSlider),
		juce::GridItem(adsrDecayLabel), juce::GridItem(adsrDecaySlider),
		juce::GridItem(adsrSustainLabel), juce::GridItem(adsrSustainSlider),
		juce::GridItem(adsrReleaseLabel), juce::GridItem(adsrReleaseSlider)
	};

	grid13.performLayout(area.removeFromTop(minItemHeight * 4));

}
