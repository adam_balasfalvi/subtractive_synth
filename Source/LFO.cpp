/*
  ==============================================================================

    LFO.cpp
    Created: 15 Jul 2021 5:14:03pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#include "LFO.h"

/// <summary>
/// Prepares the LFO: set the desired specifications, and calls the init method of the LFO.
/// </summary>
/// <param name="spec">The specifications of the oscillator. It contains the maximum block size, number of channels and sample rate.</param>
void LFO::prepareToPlay(juce::dsp::ProcessSpec& spec)
{
    prepare(spec);
    LFO::sampleRate = spec.sampleRate;
    initLFO();
}

/// <summary>
/// Sets the parameters of the LFO.
/// </summary>
/// <param name="freqParam">The desired frequency.</param>
/// <param name="amplitudeParam">The desired amplitude.</param>
void LFO::setWave(const float freqParam, const float amplitudeParam)
{
    freq = freqParam;
    amplitude = amplitudeParam;
    setFrequency(freq);
}

/// <summary>
/// Calculates the modulation based on the oscillator frequency and amplitude.
/// </summary>
/// <returns>The modulation rate.</returns>
float LFO::calculateModulation()
{
    // The processSample method calculates a single sample, the method's parameter is added as an offset to the calculated sample.
    return processSample(0) * amplitude;
}

/// <summary>
/// Resets the LFO.
/// </summary>
void LFO::reset()
{
    Oscillator::reset();
}

/// <summary>
/// Gets the LFO's sample rate.
/// </summary>
/// <returns>The sample rate.</returns>
float LFO::getSampleRate() const
{
    return LFO::sampleRate;
}

/// <summary>
/// Gets the LFO's frequency.
/// </summary>
/// <returns>The actual frequency.</returns>
float LFO::getFreq() const
{
    return LFO::freq;
}

/// <summary>
/// Gets the LFO's amplitude.
/// </summary>
/// <returns>The actual amplitude.</returns>
float LFO::getAmplitude() const
{
    return LFO::amplitude;
}

/// <summary>
/// Initialises the oscillator with a sinusodial waveform.
/// </summary>
void LFO::initLFO()
{
    initialise([](float x) { return std::sin(x); });
}