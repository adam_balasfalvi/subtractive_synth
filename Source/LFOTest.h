/*
  ==============================================================================

    LFOTest.h
    Created: 1 Nov 2021 3:23:53pm
    Author:  Balasfalvi Adam

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "LFO.h"

class LFOTest : public juce::UnitTest
{
public:
    LFOTest() : UnitTest("LFO testing") {}

    void runTest() override
    {
        LFO lfo;

        beginTest("LFO prepare to play");
        juce::dsp::ProcessSpec spec;
        spec.maximumBlockSize = 256;
        spec.sampleRate = 44100.0f;
        spec.numChannels = 2;
        lfo.prepareToPlay(spec);
        expectEquals(lfo.getSampleRate(), 44100.0f);

        beginTest("Set wave");
        lfo.reset();
        lfo.setWave(1000.0f, 5.0f);
        expectEquals(lfo.getFreq(), 1000.0f);
        expectEquals(lfo.getFrequency(), 1000.0f);
        expectEquals(lfo.getAmplitude(), 5.0f);
    }
};

// Creating a static instance will automatically add the instance to the array
// returned by UnitTest::getAllTests(), so the test will be included when you call
// UnitTestRunner::runAllTests()
static LFOTest lfo_test;